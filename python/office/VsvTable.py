'''''''''''''''''
Save a spreadsheet as a VSV table.


'''''''''''''''''

import os, uno
from com.sun.star.awt import MessageBoxButtons as MSG_BUTTONS
from com.sun.star.beans import PropertyValue
from scriptforge import CreateScriptService
from pathlib import Path
from vsv import vsv


DELIM = '`'
DELIMCODE = str(ord(DELIM))
NEWLINE = "\n"
SIGNATURE = "[[VSV]] [[table]]"
CTX = uno.getComponentContext()
SM = CTX.getServiceManager()

def create_instance(name, context=None):
   if context:
      instance = SM.createInstanceWithContext(name, context)
   else:
      instance = SM.createInstance(name)
   return instance

def msgbox(message, title='LibreOffice', buttons=MSG_BUTTONS.BUTTONS_OK, type_msg='infobox'):
   """ Create message box
      type_msg: infobox, warningbox, errorbox, querybox, messbox
      https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1awt_1_1XMessageBoxFactory.html
   """
   toolkit = create_instance('com.sun.star.awt.Toolkit')
   parent = toolkit.getDesktopWindow()
   mb = toolkit.createMessageBox(parent, type_msg, buttons, title, str(message))
   return mb.execute()

def openFilePicker(path=None):
   # allow opening multiple files at once
   filepicker = create_instance("com.sun.star.ui.dialogs.OfficeFilePicker")
   filepicker.initialize((0,))
   filepicker.setMultiSelectionMode(True)
   if path:
      filepicker.setDisplayDirectory(path)
   filteredNames = "*.vsv; *.von"
   filepicker.appendFilter(filteredNames, filteredNames)
   if filepicker.execute():
      return filepicker.getSelectedFiles()
   return []

def getUsedArea(sheet):
   cursor = sheet.createCursor()
   cursor.gotoEndOfUsedArea(False)
   cursor.gotoStartOfUsedArea(True)
   return cursor

def printRowVsv(sheet, rowId, nColumns):
   output = []

   colId = 0
   while colId < nColumns:
      cell = sheet.getCellByPosition(colId, rowId)
      output.append(cell.String)
      colId += 1

   if (rowId):
      s = vsv.writeData(output, "", 0)
   else:
      s = vsv.writeHeader(output, '}', 0)

   return s

def printRangeVsv(sheet, range):
   # convert cell range into VSV text
   nColumns = range.Columns.Count
   nRows = range.Rows.Count
   output = ""
   rowId = 0
   while rowId < nRows:
      output += printRowVsv(sheet, rowId, nColumns)
      rowId += 1

   return output

def saveVsv():
   # put each row of sheet into VSV file
   ctx = uno.getComponentContext()
   doc = XSCRIPTCONTEXT.getDocument()
   controller = doc.getCurrentController()
   homepath = create_instance("com.sun.star.util.PathSettings", ctx).Work
   dirname = os.path.dirname(doc.URL) or homepath
   sheet = controller.getActiveSheet()
   name = sheet.getName().lower().replace(' ', '-')
   filename = '{0}/{1}.vsv'.format(dirname, name)

   # convert cell range data into VSV text
   output = SIGNATURE + NEWLINE
   output += printRangeVsv(sheet, getUsedArea(sheet))

   # write output to file
   fs = CreateScriptService("FileSystem")
   file = fs.CreateTextFile(filename, True)
   file.WriteLine(output)
   file.CloseFile()
   file = file.Dispose()

   msgbox("Saved to : " + filename)

def exportVsv(*args):
   # save current sheet to VSV file
   saveVsv()
   #saveCsvVsv()
   return

def insertVsvData(sheet, vsvData):
   # vsvData is array of lines
   rowId = 0
   lines = vsvData.contents
   lines.pop(0) # ignore signature row
   for line in lines:
      colId = 0

      # determine header or data
      rowType = line.pop(0)
      for field in line:
         cell = sheet.getCellByPosition(colId, rowId)
         cell.String = field
         if rowType == vsv.HEADER:
            cell.CellStyle = "Heading 2"
         elif rowType == vsv.DATA:
            cell.CellStyle = "Default"

         colId += 1

      rowId += 1

def importVsvSheet(doc, filename):
   # create new sheet with same name as file stem
   sheetName = Path(filename).stem
   if sheetName not in doc.Sheets:
      doc.Sheets.insertNewByName(sheetName, 999)
   sheet = doc.Sheets[sheetName]
   doc.getCurrentController().setActiveSheet(sheet)

   # load data into vsv object
   vsvData = vsv(s=filename, file=True)
   insertVsvData(sheet, vsvData)


def importVsv(*args):
   # open file dialog
   # allow multiple files at once, iteration
   doc = XSCRIPTCONTEXT.getDocument()
   files = openFilePicker()
   for file in files:
      importVsvSheet(doc, file)

   msgbox("Imported {0} sheets".format(len(files)))
   return


g_exportedScripts = [exportVsv,importVsv]
