'''
= VSV = Versatile Separated Values

VSV is superset of comma separated values, tab separate values, and other SV.
VSV can use any character as delimiter, except space and newline.
VSV allows header fields, like those used in tables.
VSV can emulate other complex structures, like JSON, XML, playlists, subtitles, etc.
VSV is very simple to decode, encode, and manipulate.
Hence very easy to implement in any programming language, including
Python, Lua, Javascript, PHP, C/C++/C#, etc.

For more information on VSV, visit this URL:
https://ieants.cc/smf/index.php?topic=203.0

For source code in various languages, visit:
https://bitbucket.org/Shenafu/vsv/src/master/

VSV is Public Domain. It aspires to be an open standard.
No royalties are necessary to use or distribute VSV.

'''

'''
This version is requires io to read/write to filesystem.
'''

import re
import io

class vsv:
	# row types
	HEADER = 'HEADER'
	DATA = 'DATA'
	FILETYPE = 'FILETYPE'
	NEWLINE = "\n"
	SIGNATURE = "[[VSV]]"

	# regexp patterns to match header rows and fields
	FIELDOPENER = re.compile(r'^[\[\(\{\<]{2}')
	FIELDBRACKETS = re.compile(r'(\{{2}(.*?)\}{2}|\({2}(.*?)\){2}|\[{2}(.*?)\]{2}|\<{2}(.*?)\>{2})')
	DEFAULTDELIMS = "|`:;=,.^#$@_*/%~-"; # default delimiters
	DEFAULTBRACKETS = '[](){}<>';

	def __init__(self, s=None, file=False):
		'''
		`s` is the string message, or the file name, to be decoded.
		'''
		self.s = s

		'''
		`file` is True if decoding a file. Then `s` is the file name
		from which data will be read.
		'''
		self.file = file

		'''
		`contents` holds the data after being decodeed by self.decode(). It is a nested list, where each item is parsed
		from each line of the string or file. After contents is stored,
		users can manipulate at will.
		e.g. convert contents into json
		'''
		self.decode(file=file, save=True) # `save` is True will save to self.contents

		'''
		`hasSignature` is True if the file leads with SIGNATURE
		'''

	def fileHasSignature(self, buffer):
		oldOffset = buffer.tell()
		buffer.seek(0)
		self.hasSignature = vsv.SIGNATURE == buffer.read(len(vsv.SIGNATURE))
		buffer.seek(oldOffset)
		return self.hasSignature

	def decode(self, s=None, file=False, save=False):
		if (s):
			self.s = s
		s = self.s

		# create IO buffer for string or file
		if (file):
			try:
				print("Opening file: {}".format(s))
				buf = open(s, 'rU')
			except:
				print("Cannot open file: {}".format(s))
				print('Error Reading VSV.')
				return
		else:
			try:
				buf = io.StringIO(unicode(s))
			except:
				print("Cannot read string.")
				print("Error Reading VSV.")
				return

		# parse each line from buffer
		output = []
		try:
			buf.seek(0)
			if (self.fileHasSignature(buf)):
				print("File has VSV signature.")
				buf.readline()
			for line in buf:
				line = unicode(line)
				line = line.strip(' ')
				line = line.strip('\n')
				#print(line)
				if (len(line)):
					if (vsv.FIELDOPENER.match(line)):
					# is header row
						matches = vsv.FIELDBRACKETS.findall(line)
						if (len(matches)):
							# get fields inside matched brackets
							matches = list(map(lambda x: x[1] + x[2] + x[3] + x[4], matches))
							matches.insert(0, vsv.HEADER)
						else:
							matches = None
					else:
						# is data row
						delimiter = line[0]
						#print(delimiter)
						line = line[1:]
						if (line[-1:]==delimiter):
							# disregard delimiter at end of line
							line = line[:-1]
						matches = line.rsplit(delimiter)
						if (len(matches)):
							matches.insert(0, vsv.DATA)
						else:
							matches = None

					if (matches):
						output.append(matches)

		except Exception as e:
			print("Error decoding buffer.")
			print("Error Reading VSV.")
			print(e)
			return

		buf.close()

		if (save):
			self.contents = output

		return output

	@staticmethod
	def getBracketAt(n):
		return vsv.DEFAULTBRACKETS[n]

	@staticmethod
	def getBracketIndex(br):
		dbr = vsv.DEFAULTBRACKETS
		nbr = dbr.index(br[0]) if br and br[0] in dbr else 0
		if (1 == nbr % 2):
			nbr -= 1

		return nbr

	@staticmethod
	def writeField(text, br='['):
		# br should be desired bracket

		# find the right brackets
		nbr = vsv.getBracketIndex(br);
		bFound = True
		dbrLen = len(vsv.DEFAULTBRACKETS)
		i = dbrLen
		while (i):
			obr = vsv.getBracketAt(nbr)
			cbr = vsv.getBracketAt(nbr+1)
			if (obr not in text and cbr not in text):
				break
			nbr = (nbr + 2) % dbrLen
			i -= 1

		return obr*2 + text + cbr*2

	@staticmethod
	def writeHeader(arr, obr="[", indent=0):
		# arr should be array
		# obr should be desired open bracket
		# indent is indentation level. will be replaced with that many spaces

		s = ' ' * indent
		spacing = ' ' * max(indent, 1);
		for field in arr:
			s += vsv.writeField(field, obr)
			s += spacing

		return s + vsv.NEWLINE

	@staticmethod
	def findDelim(text, delims):
		for delim in delims:
			if (delim not in text):
				return delim

		# error , No delimiter usable for this string

	@staticmethod
	def writeData(arr, delims="", indent=0):
		# arr should be array
		# delims should be string
		# indent should be number

		delims += vsv.DEFAULTDELIMS
		delim = vsv.findDelim(str(arr), delims)

		s = ""
		s += ' ' * indent
		for item in arr:
			s += delim + item

		s += vsv.NEWLINE
		return s

	@staticmethod
	def writeVsv(arr, hasSignature):
		s = ""
		if (hasSignature):
			s += vsv.SIGNATURE + vsv.NEWLINE
		for row in arr:
			rowType = row.pop(0)
			if rowType == vsv.HEADER:
				s += vsv.writeHeader(row, "[", 0)
			elif rowType == vsv.DATA:
				s += vsv.writeData(row, "", 0)

		return s

	def writeVsvFromContents(self):
		return vsv.writeVsv(self.contents, self.hasSignature)