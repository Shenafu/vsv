
(function() {

	function createTitle(title) {
		var newEl = document.createElement("p");
		newEl.innerText = title || "Input";
		newEl.classList.add('title');
		return newEl;
	}

	function parseVsvEls() {
		Array.prototype.forEach.call(document.querySelectorAll("[class^=vsv2]"), function (el) {
			var text = el.innerHTML;
			var formatType = el.className.match( /vsv2([^ ]+)/ )[1];

			if (VSV.mapTo[formatType]) {
				var newEl = VSV.mapTo[formatType](text);

				switch (formatType) {
					case 'json':
					case 'von':
						var obj = newEl;
						newEl = document.createElement("p");
						newEl.innerText = (formatType == "json") ? obj : JSON.stringify(obj, null, "   ");
						break;
					case 'list':
					case 'table':
					case 'xml':
					case 'vml':
					case 'default':
				}

				if (bVsvReplace) {
					(newEl) ? el.parentNode.replaceChild(newEl, el) : null;
				}
				else {
					newEl.className = "vsv" + formatType;
					(newEl) ? el.insertAdjacentElement('afterend', newEl) && el.insertAdjacentElement('afterend', createTitle("Output:")) && el.insertAdjacentElement('beforebegin', createTitle("VSV code")) : null;
				}
			}
		});
	}

window.addEventListener('load', parseVsvEls, false);
})();