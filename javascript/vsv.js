/*
VSV = Versatile Separated Values
Interpret VSV markup into lists, tables, HTML, and JSON

Public Domain - Open standard - No royalty
//*/

var log = console.log;

String.prototype.repeat = String.prototype.repeat ? String.prototype.repeat : function(times) {
	var ret = '';
	for (var i=0; i<times; i++) {
		ret += this;
	}
	return ret;
};

function htmlEntitiesDecode(str) {
    return String(str).replace(/&amp;/g, '&').replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&quot;/g, '"');
}

var VSV = VSV || { "mapTo":{} };
if (typeof module !== 'undefined') {
	module.exports=VSV;
}
VSV.HEADER = {rowtype:'HEADER'};
VSV.DATA = {rowtype:'DATA'};
VSV.FILETYPE = {rowtype:'FILETYPE'};
VSV.fieldOpener = /^[\[\(\{\<]{2}/;
VSV.fieldBrackets = /(\{{2}(.*?)\}{2}|\({2}(.*?)\){2}|\[{2}(.*?)\]{2}|\<{2}(.*?)\>{2})/g;
VSV.captionSplit = '``';
VSV.captionHeader = new RegExp('^' + VSV.captionSplit);
VSV.defaultDelims = ",.-=:`|;"; // default delimiters
VSV.defaultBrackets = '[](){}<>';

VSV.getBracketAt = function(n) {
	return VSV.defaultBrackets.charAt(n);
}

VSV.writeField = function(text, br) {
	// br should be desired open bracket
	// e.g. '['

	var dbr = VSV.defaultBrackets;
	var nbr = br ? Math.max(dbr.indexOf(br.charAt(0)), 0) : 0;
	if (nbr % 2 == 1) {
		// turn close bracket into open bracket
		nbr--;
	}
	var obr, cbr;

	//verify obr and cbr not found in text
	var found = true;
	for (var i=0, len=dbr.length/2; i<len; i++) {
		obr = VSV.getBracketAt(nbr);
		cbr = VSV.getBracketAt(nbr+1);

		if (text.indexOf(obr) == -1 && text.indexOf(cbr) == -1) {
			// bracket not found in text, can safely exit loop
			found = false;
			break;
		}

		nbr = (nbr + 2) % dbr.length;
	}

	if (found) {
		throw new Error("No Bracket usable for this string: " + text);
	}

	return (obr + obr + text + cbr + cbr);
}

VSV.writeHeader = function(arr, obr, indent) {
	// arr should be array
	// obr should be desired open bracket

	var dbr = VSV.defaultBrackets;
	var nbr = obr ? Math.max(dbr.indexOf(obr.charAt(0)), 0) : 0;
	if (nbr % 2 == 1) {
		// turn close bracket into open bracket
		nbr--;
	}
	br = VSV.getBracketAt(nbr);
	indent = indent || 0;
	var s = '';

	s += ' '.repeat(indent);
	indent = Math.max(indent, 1);
	var spacing = ' '.repeat(indent);

	for (var k in arr) {
		try {
			s += VSV.writeField(arr[k], br);
		} catch(err) {
			log(err);
		}
		s += spacing;
	}
	s += '\n';

	return s;
}

VSV.findDelim = function(text, delims) {
	//verify delimiter not found in text

	var del;
	var found = true;
	for (var i=0, len=delims.length; i<len; i++) {
		del = delims.charAt(i);

		if (text.indexOf(del) == -1) {
			// delimiter not found in text, can safely exit loop
			found = false;
			break;
		}
	}

	if (found) {
		throw new Error("No Delimiter usable for this string: " + text);
	}

	return del;
}

VSV.writeData = function(arr, delims, indent) {
	// arr should be array
	// delims should be string
	// indent should be number

	delims = (delims || '') + VSV.defaultDelims;
	var del;
	try {
		del = VSV.findDelim(arr.join(''), delims);
	} catch(err) {
		log(err);
		return '';
	}

	var s = '';
	s += ' '.repeat(indent);
	for (var k in arr) {
		var t = arr[k] === 'undefined' ? '' : arr[k];
		s += del + t;
	}
	s += '\n';

	return s;
}

VSV.mapTo.array = function(text, keepDelimiter) {
	// convert text rows into array
	// each array item is subarray of header or data items
	// each subarray's 0th index is VSV.HEADER or VSV.DATA

	var vsvArray = [];
	var countRows = 0;
	var rows = htmlEntitiesDecode(text).split("\n");
	rows.forEach(function(row) {
		row = row.replace( /(^ *| *$)/, ''); // trim spaces only

		if (row.length == 0) {
			return;
		}

		var matches = null;

		if (VSV.fieldOpener.test(row)) {
			// is header row
			// get all fields into array
			matches = row.match(VSV.fieldBrackets);

			if (matches) {
				for (var i=0; matches[i]; i++) {
					matches[i] = matches[i].replace(VSV.fieldBrackets, "$2$3$4$5");
				}
				if (0==countRows && VSV.vsvElements.hasOwnProperty(matches[0])) {
					matches.unshift(VSV.FILETYPE);
				}
				else {
					matches.unshift(VSV.HEADER);
				}
			}
		}
		else {
			// is data row
			var delimiter = row.substring(0, 1);
			row = row.substring(1); // ignore first delimiter
			// ignore if last character is delimiter
			if (row.charAt(row.length-1)==delimiter) {
				row = row.substring(0, row.length-1);
			}
			matches = row.split(delimiter);
			if (matches) {
				if (keepDelimiter) {
					matches.unshift(delimiter);
				}
				matches.unshift(VSV.DATA);
			}
		}
		(matches) ? vsvArray.push(matches) : null;
		countRows++;
	});

	return vsvArray;
}

VSV.mapTo.list = function(vsv) {
	if (typeof vsv == "string") {
		vsv = VSV.mapTo.array(vsv);
	}

	var list = document.createElement("ul");
	var stack = [list]; // nested list tree
	var useHeaders = false; // nested list with or without headers
	var lastTier = 0; // nested list last parent = stack[lastTier]

	// parse by row
	vsv.forEach(function(row) {
		var rowType = row.shift();

		switch (rowType) {
			case VSV.HEADER:
				// is header row
				// get all fields into list
				useHeaders = true;
				var tier = 1;
				var newHeader = false;
				row.forEach(function(header) {
					if (header.length == 0) {
						tier++;
					}
					else {
						var label = document.createElement("li");
						label.innerHTML = header;
						var newList = document.createElement("ul");
						label.appendChild(newList);

						if (tier > lastTier) {
							stack[lastTier].appendChild(label);
							stack.push(newList);
							lastTier++;
						}
						else if (tier < lastTier) {
							while (tier <= lastTier) {
								stack.pop();
								lastTier--;
							}
							stack[lastTier].appendChild(label);
							stack.push(newList);
						}
						else {
							stack.pop();
							stack[lastTier-1].appendChild(label);
							stack.push(newList);
						}

						newHeader = true;
					}
				});

				// row with only empty headers will return to that many previous tiers
				if (!newHeader) {
					while (tier > 1 && lastTier) {
						stack.pop();
						lastTier--;
						tier--;
					}
				}
				break;
			case VSV.DATA:
				// is data row
				var tier = 0;
				row.forEach(function(data) {
					if (useHeaders) {
						// headers as indentation
						var item = document.createElement("li");
						item.innerHTML = data;
						stack[lastTier].appendChild(item);
					}
					else {
						// repeated delimiters as indentation
						if (data.length == 0) {
							tier++;
						}
						else {
							var item = document.createElement("li");
							item.innerHTML = data;
							if (tier > lastTier) {
								lastTier++;
								if (!stack[lastTier]) {
									// empty list, just started
									stack[--lastTier].appendChild(item);
									stack[lastTier+1] = item;
								}
								else if (stack[lastTier].nodeName === "LI") {
									var newList = document.createElement("ul");
									newList.appendChild(item);
									stack[lastTier-1].appendChild(newList);
									stack[lastTier] = newList;
									stack[lastTier+1] = item;
								}
								else {
									stack[lastTier] = item;
								}
							}
							else if (tier < lastTier) {
								while (!stack[tier]) {
									tier--;
								}
								lastTier = tier;
								stack[lastTier].appendChild(item);
								stack[lastTier+1] = item;
							}
							else {
								stack[lastTier].appendChild(item);
								stack[lastTier+1] = item;
							}
						}
					}
				});
				break;
		}
	});

	return list;
}

VSV.mapTo.tableHeader = function(vsv, table) {
	// process a single header row for a table
	if (!table || "TABLE" != table.nodeName) {
		return;
	}
	if (typeof vsv == "string") {
		vsv = VSV.mapTo.array(vsv)[0];
		vsv.shift();
	}
	// get all fields into table row
	var tr = document.createElement('tr');
	vsv.forEach(function(header) {
		var captions = header.trim().match(VSV.captionHeader);
		if (captions) {
			// is caption
			// 1st field = title
			// 2nd field = position
			captions = header.trim().split(VSV.captionSplit);
			captions.shift();
			var caption = document.createElement("caption");
			table.appendChild(caption);
			caption.textContent = captions.shift();
			if (captions.length) {
				var poss = captions.shift().match(/./g);
				while(poss && poss.length) {
					var pos = poss.shift();
					switch(pos) {
						case 'l':
							caption.style.textAlign = "left";
							break;
						case 'c':
							caption.style.textAlign = "center";
							break;
						case 'r':
							caption.style.textAlign = "right";
							break;
						case 't':
							caption.style.captionSide = "top";
							break;
						case 'b':
							caption.style.captionSide = "bottom";
							break;
						default:
					}
				}
			}
		} else {
			var th = document.createElement("th");
			tr.appendChild(th);
			th.innerHTML = header;
		}
	});
	if  (tr.childElementCount) {
		table.appendChild(tr);
	}
	return table;
}

VSV.mapTo.tableData = function(vsv, table) {
	// process a single data row for a table
	if (!table || "TABLE" != table.nodeName) {
		return;
	}
	if (typeof vsv == "string") {
		vsv = VSV.mapTo.array(vsv)[0];
		vsv.shift();
	}
	// get all values into table row
	var tr = document.createElement('tr');
	vsv.forEach(function(data) {
		var td = document.createElement("td");
		tr.appendChild(td);
		td.innerHTML = data.replace(/\\n/g, "\n");
	});
	if  (tr.childElementCount) {
		table.appendChild(tr);
	}
	return table;
}

VSV.mapTo.table = function(vsv) {
	if (typeof vsv == "string") {
		vsv = VSV.mapTo.array(vsv);
	}
	var table = document.createElement("table");
	// parse by row
	vsv.forEach(function(row) {
		var rowType = row.shift();
		switch (rowType) {
			case VSV.HEADER:
				// is header row
				VSV.mapTo.tableHeader(row, table);
				break;
			case VSV.DATA:
				// is data row
				VSV.mapTo.tableData(row, table);
				break;
		}
	});
	return table;
}

VSV.mapTo.xml = function(vsv) {
	if (typeof vsv == "string") {
		vsv = VSV.mapTo.array(vsv);
	}

	var xml = document.createElement("div");
	var stack = [xml]; // DOM tree
	var currTag = xml, lastTag = xml;

	// parse by row
	vsv.forEach(function(row) {
		var rowType = row.shift();

		switch (rowType) {
			case VSV.HEADER:
				// is header row
				// get all tags into DOM tree
				row.forEach(function(tag) {

					// determine if opener or closer
					switch (tag) {
						case '/':
							// close current tag and retrieve last tag
							currTag = lastTag = stack.pop();
							break;
						default:
							// add new tag to tree
							stack.push(currTag);
							lastTag = currTag;
							currTag = document.createElement(tag);
							lastTag.appendChild(currTag);
					}
				});
				break;
			case VSV.DATA:
				// is data row
				var key = row[0], value = row[1];

				// attribute has key and value
				if (value) {
					currTag.setAttribute(key, value);
				}
				else {
					currTag.appendChild(document.createTextNode(key));
				}
				break;
		}
	});

	return xml;
}

VSV.mapTo.json = function(vsv) {
	if (typeof vsv == "string") {
		vsv = VSV.mapTo.array(vsv);
	}

	var json = "";
	var bArray = false;
	var bObject = false;

	// parse by row
	vsv.forEach(function(row) {
		var rowType = row.shift();

		switch (rowType) {
			case VSV.HEADER:
				// is header row
				// get all fields into array
				row.forEach(function(field) {
					var bracketOpen = field.substring(0, 1);
					var bracketClose = field.substring(field.length-1);

					// determine if object or array, or closing either
					switch (field) {
						case '':
							// unnamed object/array
							json += bracketOpen + " ";
							break;
						case ';':
							// remove extra commas
							json = json.replace( /, *$/, "");
							// close object/array
							json += bracketClose + ", ";
							break;
						default:
							// named object/array
							json += '"' + field + '": ' + bracketOpen + ' ';
					}
				});
				break;
			case VSV.DATA:
				// is data row
/*/
				if (curr.length != undefined) {
					// row is in the form ,data,data,data
					foreach () {
						json += ','
					}
				}
				else {
					//*/
					var key = row[0], value = row[1];
					// assign "key": "value"
					json += '"' + key + '": "' + value + '", ';
				//}
				break;
		}
	});

	// remove extra commas
	json = json.replace( /, *$/, "");

	// debug
	// test if valid JSON
	//var obj = JSON.parse(json);

	return json;
}

VSV.mapTo.vml = function(von) {
	if (typeof von == "string") {
		von = VSV.mapTo.von(von, []);
	}
	var ret = document.createElement('div');
	VSV.mapTo.vmlRecurse(ret, von);
	return ret.firstChild;
}

VSV.mapTo.vmlRecurse = function(parent, von) {
	var ret = null;
	var tag = von && von.tag;

	if (tag) {

		if (VSV.vsvElements.hasOwnProperty(tag)) {
			// remember to join header rows
			von.headers = von.headers || [];
			von.tots = von.headers.concat(von.tots);

		}
		// document element
		var el = VSV.vsvElements.hasOwnProperty(tag) ? VSV.vsvElements[tag](von.tots) : document.createElement(tag);

		// special treatments for certain tags
		//*
		switch (tag) {
			case 'ftable':
				// fill table cell by cell, row by row
				von.atts.columns = von.atts.columns || 2;
				von.atts.headerRows = von.atts.headerRows || 0;
				von.atts.currCol = 0;
				von.atts.currHeaderRow = 0;
				el = document.createElement('table');
				break;

			default:
				break;
		}

		// apply attributes
		for (var key in von.atts) {
			if (key && von.atts.hasOwnProperty(key)) {
				// ignore certain atts that don't belong in DOM
				// or apply special methods, like style
				switch (key) {
					case 'type':
					case 'tier':
					case 'tag':
						break;

					case 'style':
						for (var property in von.atts[key]) {
							// convert property-name to propertyName
							var styleName = property.replace("/-(.)/g", function(v) { return v.toUpperCase(); });
							el.style[styleName] = von.atts[key][property];
						}
						break;

					default:
						el.setAttribute(key, von.atts[key]);
						break;
				}
			}
		}

		// add children
		if (!VSV.vsvElements[tag]) {
			while(von.tots.length) {

				// special treatments for certain tags
				switch (tag) {
					case 'ftable':
						// fill table cell by cell, row by row

						if (VSV.HEADER == von.tots[0][0]) {
							VSV.mapTo.tableHeader(von.tots[0].slice(1), el);
							von.tots.shift();

						}
						else {
							// create new row
							if (0 == von.atts.currCol) {
								var row = document.createElement('tr');
								el.appendChild(row);
							}

							// fill headers first
							if (von.atts.headerRows > von.atts.currHeaderRow) {
								var cell = document.createElement('th');
							}
							else {
								// fill data rows
								var cell = document.createElement('td');
							}
							row.appendChild(cell);
							VSV.mapTo.vmlRecurse(cell, von.tots.shift());
							von.atts.currCol++;
							if (von.atts.columns == von.atts.currCol) {
								(von.atts.headerRows > von.atts.currHeaderRow) ? von.atts.currHeaderRow++ : null;
								von.atts.currCol = 0;
							}
						}
						continue;

					default:
						break;
				}

				VSV.mapTo.vmlRecurse(el, von.tots.shift());
			}
		}
		parent.appendChild(el);
	}
	else {
		// text node
		parent.appendChild(document.createTextNode(von));
	}
}

VSV.mapTo.von = function(vsv, basetype) {
	basetype = basetype || {};
	// convert VON into hiearchical array or object recursively
	if (typeof vsv == "string") {
		vsv = VSV.mapTo.array(vsv, true);
	}
	var ret = VSV.mapTo.vonRecurse({atts:{tier:parseInt("-1")}, tots:basetype, vontype:Array.isArray(basetype) ? "array" : "object"}, vsv);
	if (Array.isArray(basetype)) {
		return ret.tots[0];
	}
	else {
		return ret.tots[Object.keys(ret.tots)[0]];
	}
}

VSV.mapTo.vonRecurse = function(parent, rows, bAttr) {
	bAttr = bAttr || false;
	var ret = null;
	while(rows.length) {
		var row = rows.shift();
		var rowType = row.shift();
		switch (rowType) {
			case VSV.HEADER:
				// for VSV-style tables and lists
				// header rows are saved as subarray
				row.unshift(rowType);
				if ("array"==parent.vontype) {
					parent.tots.push(row);
				}
				else if ("object"==parent.vontype) {
					parent.tots.headers = parent.tots.headers || [];
					parent.tots.headers.push(row);
				}
				break;

			case VSV.DATA:
					// is data row
				delim = row.shift();
				switch (delim) {
					case '}':
					case ']':
					case ')':
						// close current container
						if (delim == ')') {
							parent += " }";
						}
						return parent;

					case '{':
					case '[':
					case '(':
						// add new object or array or function to tree
						if ('{' == delim) {
							if ('}' == row[0].slice(-1)) {
								// inline object
								var list = row[0].replace(/^ */, '').slice(0, -1);
								var arrayDelim = list.slice(0,1);
								var subArray = list.split(arrayDelim);
								subArray.shift();
								if (bAttr) {
									var newObject = {};
									var newKey = subArray.shift();
									parent.atts[newKey] = newObject;
									// each pair in subArray is (key, value)
									for (var key,i=0; key=subArray[i]; i=i+2) {
										newObject[key] = subArray[i+1];
									}
								}
								else {
									var newObject = {atts:{tier:parseInt(parent.atts.tier)+1}, tots:{}, vontype:"object"};
									// if parent is object, first data is key name
									var newKey = subArray.shift();
									if ("object"==parent.vontype) {
										parent.tots[newKey] = newObject;
									}
									// each pair in subArray is (key, value)
									for (var key,i=0; key=subArray[i]; i=i+2) {
										newObject.tots[key] = subArray[i+1];
									}
									if ("array"==parent.vontype) {
										parent.tots.push(newObject);
									}
								}
								break;
							}
							else {
								if (bAttr) {
									var subArray = VSV.mapTo.vonRecurse({atts:{}}, rows, bAttr).atts;
									if (Array.isArray(parent.atts)) {
										parent.atts.push(subArray);
									}
									else {
										parent.atts[row.shift()] = subArray;
									}
									break;
								}
								else {
									ret = VSV.mapTo.vonRecurse({atts:{tier:parseInt(parent.atts.tier)+1}, tots:{}, vontype:"object"}, rows, bAttr);
								}
							}
						}
						else if ('[' == delim) {
							if (']' == row[0].slice(-1)) {
								// inline array
								var list = row[0].replace(/^ */, '').slice(0, -1);
								var arrayDelim = list.slice(0,1);
								var subArray = list.split(arrayDelim);
								subArray.shift();
								if (bAttr) {
									var newKey = subArray.shift();
									parent.atts[newKey] = subArray;
								}
								else {
									subArray = {atts:{tier:parseInt(parent.atts.tier)+1}, tots:subArray, vontype:"array"};
									// if parent is object, first data is key name
									if ("array"==parent.vontype) {
										parent.tots.push(subArray);
									}
									else {
										var key = subArray.tots.shift();
										parent.tots[key] = subArray;
									}
								}
								break;
							}
							else {
								if (bAttr) {
									var subArray = VSV.mapTo.vonRecurse({atts:[]}, rows, bAttr).atts;
									if (Array.isArray(parent.atts)) {
										parent.atts.push(subArray);
									}
									else {
										parent.atts[row.shift()] = subArray;
									}
									break;
								}
								else {
									var newArray = {atts:{tier:parseInt(parent.atts.tier)+1}, tots:[], vontype:"array"};
									("vsv"==row[0].slice(0, 3)) ? newArray.bVsv = true : null ;
									ret = VSV.mapTo.vonRecurse(newArray, rows, bAttr);
								}
							}
						}
						else if ('(' == delim) {
							ret = VSV.mapTo.vonRecurse("function " + row[0] + "() {", rows, bAttr);
						}

						// append to parent container
						if ("array"==parent.vontype) {
							// parent is array
							if (typeof ret !== 'string') {
								ret.tag = row[0];
							}
							parent.tots.push(ret);
						}
						else if ("object"==parent.vontype) {
							// parent is object
							if (parent.tots) {
								parent.tots[row[0]] = ret;
							}
						}
						break;

					case '<':
						if ('>' == row[0].slice(-1)) {
							if (typeof parent === 'string') {
								// add arguments to function
								var insertIndex = parent.indexOf(")");
								var args = "";
								var list = row[0].replace(/^ */, '').slice(0, -1);
								var arrayDelim = list.slice(0,1);
								var subArray = list.split(arrayDelim);
								subArray.shift();
								subArray.forEach(function(i) {
									args += ", " + i;
								});
								if ('(' == parent.charAt(insertIndex-1)) {
									args = args.slice(2);
								}
								parent = parent.slice(0, insertIndex) + args + parent.slice(insertIndex);
							}
							else {
								// pairs of (key, value) added to atts
								var list = row[0].replace(/^ */, '').slice(0, -1);
								var arrayDelim = list.slice(0,1);
								var subArray = list.split(arrayDelim);
								subArray.shift();
								for (var key,i=0; key=subArray[i]; i=i+2) {
									parent.atts[key] = subArray[i+1];
								}
							}
						}
						else {
							bAttr = true;
						}
						break;

					case '>':
						bAttr = false;
						break;

					default:
						if (bAttr) {
							if (typeof parent === 'string'){
								// add arguments to function
								var insertIndex = parent.indexOf(")");
								var args = "";
								row.forEach(function(i) {
									args += ", " + i;
								});
								if ('(' == parent.charAt(insertIndex-1)) {
									args = args.slice(2);
								}
								parent = parent.slice(0, insertIndex) + args + parent.slice(insertIndex);
							}
							else if (Array.isArray(parent.atts)) {
								row.forEach(function(data) {
									parent.atts.push(data);
								});
							}
							else {
								parent.atts[row[0].trim()] = row[1];
							}
							break;
						}
						else if (parent.bVsv) {
							// base VSV element, like table or list
							// save entire row as subarray
							row.unshift(rowType);
							if ("array"==parent.vontype) {
								parent.tots.push(row);
							}
							else if ("object"==parent.vontype) {
								parent.tots.data = parent.tots.data || [];
								parent.tots.data.push(row);
							}
						}
						else {
							if (typeof parent === 'string') {
								// parent is function, append data to end
								parent += row.join();
								break;
							}
							else if ("array"==parent.vontype) {
								// parent is array, so append each data
								// where row is in the form
								row.forEach(function(data) {
									parent.tots.push(data);
								});
								break;
							}
							else if ("object"==parent.vontype) {
								// parent is object, add (key, value)
								var key = row.shift();
								parent.tots[key] = (row.length > 0) ? row.join(', ') : "";
								break;
							}
						}
				} // end switch delim

				break;
		} // end switch rowType
	}

	return parent;
}

VSV.vsvElements = {
	"vsvtable": VSV.mapTo.table
	,"vsvlist": VSV.mapTo.list
	,"vsvvml": VSV.mapTo.vml
	,"vml": VSV.mapTo.vml
};

VSV.mapTo.auto = function(vsv) {
	// automatically detect type of file
	// vsvtable, vsvlist, vml
	var ret;
	var firstLine = vsv.split('\n', 1)[0];
	var filetype = firstLine.match(VSV.fieldOpener) && firstLine.replace(VSV.fieldBrackets, "$2$3$4$5");
	if (filetype && VSV.vsvElements.hasOwnProperty(filetype)) {
		ret = VSV.vsvElements[filetype](vsv);
	}
	else {
		ret = VSV.mapTo.vml(vsv);
	}

	return ret;
};

VSV.mapTo.autoScript = function(vsv) {
	// insert into DOM before the current script element
	document.scripts[document.scripts.length-1].parentElement.insertBefore(VSV.mapTo.auto(vsv), document.scripts[document.scripts.length-1]);

}

VSV.pugFilters = {
	'vsv':function (text, options) {
		return "<script>VSV.mapTo.autoScript(`" + text.replace(/([`{[(<])/g, '\\$1') + "`);</script>";
	}
};