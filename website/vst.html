
	<pre class="markdown">
# VST

VST stands for Versatile SubTitles. Its file extension is .vst. VST is used to supply subtitles and captioning for media players. It has simple syntax, yet powerful and extensible functionality in a small file size.

## Criteria

- simple, yet extensible
- basic features, including time start and end, text to display
- advanced features, including styling, position, angle, color, classes, scrolling (ala marquee)
- optional metadata and general options, including language and encoding, movie metadata, author and staff of subtitle (e.g. translators, timers)

The wonderful thing about VST is simple, yet extensible and universal. Thus both simple and complex subtitles can be created in VST. Simple ones will look simple; complex ones will also look simple without clutter or different look or rules. (cf. XML is a cluttered format. VTT has different syntax for different sections.)

# Sections

A subtitle file is divided into sections marked by headers (which are not case sensitive). All sections are optional. If a file begins with no headers, it is implied to begin in the Main section. A section type can appear multiple times in the same file.

VST files may have a [[VST]] header at the first bytes of the file. This is recommended in order for programs to instantly recognize it as a VST file. This header is not counted as a section.

The [[Main]] section holds the subtitles texts, timing, and additional information pertaining to that subtitle segment.

The [[General]] section holds the metadata and general options.

The [[Style]] or [[Styles]] section defines styles with the same keywords as CSS.

# Subtitle data

Data rows in the main section hold the subtitle data that will be displayed during playback of a media file. The general layout is a time or frame row, followed by one or more rows that describe a subtitle line, including text and additional optional properties, such as style and class. That means a time or frame row signals the beginning of a new subtitle segment.

Data rows holds one or more data fields or values. The first value in a data row is the name of a property. Subsequent fields or values in the row provide additional arguments to this property.

The \ backslash is the escape character for advanced features. Including new lines of a multiline subtext, and inline styling. To print the backslash itself, use double backslash \\\\. The \ must not be used as delimiter.

For example, the following is the time property, followed by the time's start and end:

=t=12:27=12:38

# Properties

Below lists the properties and their usage. New properties may yet be discovered.

## 't'

The 't' property means time, and is followed by one or two data fields that represent the start and end times of a subtitle line. The time data is in the format HH:MM:SS.mmm. HH: can be omitted to imply zero hours. .mmm can be omitted to imply zero milliseconds.

If the start time is omitted, it will be zero 00:00, or the start of the media. If the end time is omitted, the subtitle will be displayed until the end of the media.

Ex.
,t,12:27,12:38

## 'f'

The 'f' property means frame, and is followed by one or two data fields that represent the start and end frames of a subtitle line. The frame data is an integer from 0 to the last frame of the video.

If the start frame is omitted, it will be zero 0, or the start of the media. If the end frame is omitted, the subtitle will be displayed until the end of the media.

Ex.
,f,335,518

## 's'

The 's' property defines the text to be displayed.

Every additional data field starts on a new line when displayed on the video; i.e. multiline. That is, whenever a delimiter is repeated on the same data row, it starts a new line in the video. New lines can also be denoted with \n.

To prevent delimiter-value collision, it is behooved to use a delimiter not found in the particular text for that row, and generally not normally found in your language's everyday grammar.

Ex.
In both examples, the delimiter is caret ^.

Single line
^s^Stop! In the name of love!

Multiline
^s^Stop!^In the name of love!

^s^Stop!\nIn the name of love!

Here both multiline examples will display multilines of text identically.

## 'c'

The 'c' property declares the class of this particular subtitle segment. Classes are the same concept as HTML classes. So they can be used as expedient access to various features, including styling, positioning, transformation, etc. These styles are defined in the [[Style]] section of the VST file, or in a separate .css file.

Ex.
[[Style]]
^.music^font-style:italic

[[Main]]
,s,Stop! In the name of love!
,c,music

## 'func'

The 'func' properties allows passing special commands to the media player, including vendor specific options.

Ex.
'func'stayontop

# Style

All styles go in the [[Style]] or [[Styles]] section. Styles will use same keywords and values as CSS.

The first field in the row is the selector. Usually the class name preceded by a dot. Subsequent fields are the styles applied to the selector. Each field is in the format "keyword:value". Thus colon : must not be used as delimiter for style rows.

Ex.
Style a class to display text in italic:
[[Style]]
^.music^font-style:italic

Ex.
Add content before and after a subtitle:
[[Style]]
^.music::before^content: "#"
^.music::after^content: "#"

# Full Examples

## Simple subtitles
Ex.
[[VST]]
,t,01:11.704,01:13.006
^s^All right, let’s go. I’ll give you half an hour.
,t,01:16.209,01:17.777
^s^What?
,t,01:18.845,01:20.547
^s^Are you serious?
,t,01:20.547,01:23.750
^s^Look, Jerry, we have to have sex to save the friendship.
,t,01:24.951,01:29.489
^s^Sex, to *save* the friendship.
,t,01:31.057,01:35.328
^s^Well if we have to, we have to.

## Complex subtitles

Ex.
[[VST]]
[[Style]]
^.jerry^color: green
^.elaine^color: yellow

[[Main]]
,t,01:11.704,01:13.006
^s^All right, let’s go. I’ll give you half an hour.
,c,elaine
,t,01:16.209,01:17.777
^s^What?
,c,jerry
,t,01:18.845,01:20.547
^s^Are you serious?
,c,jerry
,t,01:20.547,01:23.750
^s^Look, Jerry, we have to have sex to save the friendship.
,c,elaine
,t,01:24.951,01:29.489
^s^Sex, to *save* the friendship.
,c,jerry
,t,01:31.057,01:35.328
^s^Well if we have to, we have to.
,c,jerry

Ex.
[[VST]]
[[Style]]
^.aside^vertical-align: top^margin-top: 1em
^.music^font-style: italic
^.music::before^content: "#"
^.music::after^content: "#"

[[Main]]
,t,01:31.057,01:35.328
,s,Stop! In the name of love!
,c,music

,t,01:31.057,01:35.328
^s^Stop! In the Name of Love^by Diana Ross and The Supremes
,c,aside

# TBD

TBD: additional properties, including position, color, scrolling, angle, etc.

TBD: markup for inline styling a substring of a line, including bold, underline, italicize (e.g. *, _, /). To print these exact characters, escape it with backslash (e.g. \\\*, \\\_, \\/)

TBD: properties for general options and metadata

TBD: considerations for real-time caption, streaming

TBD: embed media: images, video, audio, 3d models, transformations, effects

TBD: commands and scripting: pause, resume, include external files, shift time or frames for text and audio (e.g. fix out of sync)

TBD: plugins for media players: VLC via Lua,

TBD: develop and test VST using a HTML5 player

# Lbiraries and Plugins

TBA

	</pre>
