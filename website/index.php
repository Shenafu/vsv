<?php require_once("include.php"); ?>

<!doctype html>
<html>
<head>
<title>VSV (Versatile Separated Values)</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link href="vsv.css" rel="stylesheet" type="text/css">
<script src="/lib/markdown.js"></script>
<script src="/lib/toc.js"></script>
<script src="/lib/vsv.js?v=1.0.13"></script>
<script src="javascript/parsevsv.js"></script>
<script>
var bVsvReplace = true;
</script>
</head>

<body onload="Page_Onload();">

<?php include('navbar.php'); ?>

<div class='content-main'>

<?php
$page = isset($_GET["page"]) ? $_GET["page"] . ".html" : '';
if (file_exists($page)) {
	include($page);
}
else {
	include("home.html");
}

?>

</div>

</body>
</html>
