
	<pre class="markdown">
# VPL

VPL stands for Versatile Play List. Its file extension is .vpl. VPL is used to supply media players list of items of play. It has simple syntax, yet powerful and extensible functionality in a small file size.

## Criteria

- simple, yet extensible
- basic features, including file, title, artist, duration
- advanced features, including repeat amount, gain, media start and end times, resume from last media played, associated files (such as subtitle or lyrics)
- optional metadata and general options


# Sections

A playlist file is divided into sections marked by headers (which are not case sensitive). All sections are optional. If a file begins with no headers, it is implied to begin in the Main section. A section type can appear multiple times in the same file.

VPL files may have a [[VPL]] header at the first bytes of the file. This is recommended in order for programs to instantly recognize it as a VPL file. This header is not counted as a section.

The [[Main]] section holds the list of media, including their filenames and other specific data.

The [[General]] section holds the metadata and general options.

# Playlist data

Data rows in the main section hold the playlist data. The general layout is a file row, followed by optional rows that describe a file, including title, artist, duration, styling, and class. That means a file row signals the beginning of a new file subsection.

Some properties will hold data that will be applied for all subsequent files. So that one may apply the same title and artist for many files without the need to repeat. See [global properties](#global-properties).

Data rows holds one or more data fields or values. The first value in a data row is the name of a property. Subsequent fields or values in the row provide additional arguments to this property.

For example, the following is the file property, followed by the file's source:

=f=file:///f:/movies/It.mp4


# Properties

Below lists the properties and their usage. New properties may yet be discovered.

## 'f'

The 'f' property means file name, and is followed by a field that refers to a media file or another playlist file. The file can point to a local computer or remote address, such that the media player can access that file. As such, the file can be relative path, absolute path, or URL that can be handled by the media player.

If 'pathAll' is defined, every subsequent file is affixed by that property. The values between pathAll and f will be interweaved, beginning with pathAll's first value.

Ex.
@f@video-01.mp4


Ex.
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb

The next example will generate the same file name as previous example:
'pathAll'file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/'.rmvb
@f@Cooking.Beauties.01

Note pathAll starts the file name "file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/". Then @f@ appends it's first value "Cooking.Beauties.01". Then back to pathAll's second value ".rmvb". Thus interweaving pathAll and @f@, we get "file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb".


## 'fn'

The 'fn' property creates a list of files using sequential numbers. The fields are the starting and ending numbers. If 1st field is omitted, starting number set to 1. If 2nd field is omitted or less than starting number, end number sets to starting number. If you need to pad numbers with leading 0s, use 'pad' property before 'fn'.

Ex.
-fn-1-154

This creates all the files from between 1 and 154. If pathAll property is also defined, numbers are inserted accordingly.

Ex.
,pathAll,file:///f:/media/anime/Major/Major ,.mp4
-pad-3
-fn--154

The file names that will be added to the playlist are:
file:///f:/media/anime/Major/Major 001,.mp4
file:///f:/media/anime/Major/Major 002,.mp4
file:///f:/media/anime/Major/Major 003,.mp4
.
.
.
file:///f:/media/anime/Major/Major 152,.mp4
file:///f:/media/anime/Major/Major 153,.mp4
file:///f:/media/anime/Major/Major 154,.mp4

Because of pad 3, the numbers are actually 001, 002, ... , 099, 100, ... , 154. Without pad, the numbers will be 1, 2, 3, ... 98, 99, 100.

Moreover, pathAll and fn also interweave. Every two values in fn will be used to generate additional sequences. When the last sequence reaches the end, it will reset to its starting value and the previous sequence will increment by one.

Thus, for instance, to generate a list of files across multiple seasons of a TV series can be written in a few short lines.

Ex.
'pathAll'file:///f:/tv/Yes, Dear/Season ' ep.'.mp4
-pad--2
-fn--6--24

will generate this list:
file:///f:/tv/Yes, Dear/Season 1 ep.01.mp4
file:///f:/tv/Yes, Dear/Season 1 ep.02.mp4
file:///f:/tv/Yes, Dear/Season 1 ep.03.mp4
file:///f:/tv/Yes, Dear/Season 1 ep.04.mp4
file:///f:/tv/Yes, Dear/Season 1 ep.05.mp4
file:///f:/tv/Yes, Dear/Season 1 ep.06.mp4
.
.
.
file:///f:/tv/Yes, Dear/Season 1 ep.22.mp4
file:///f:/tv/Yes, Dear/Season 1 ep.23.mp4
file:///f:/tv/Yes, Dear/Season 1 ep.24.mp4
file:///f:/tv/Yes, Dear/Season 2 ep.01.mp4
file:///f:/tv/Yes, Dear/Season 2 ep.02.mp4
file:///f:/tv/Yes, Dear/Season 2 ep.03.mp4
.
.
.
file:///f:/tv/Yes, Dear/Season 6 ep.22.mp4
file:///f:/tv/Yes, Dear/Season 6 ep.23.mp4
file:///f:/tv/Yes, Dear/Season 6 ep.24.mp4


Thus you can see that after Season 1 ep.24, the Season number is raised and the episode number is reset to 01. Thus all 6 seasons of "Yes, Dear" can be played using just these three short lines of code in VPL.

Also note, that the first sequence is padded to 1 digit, but the second sequence is padded to 2 digits.

## 'pad'

The 'pad' property pads numbers with leading 0s for 'fn' property. That is, if the amount of digits in a number are shorter than pad, then extra 0s are added to the left until the number has that many digits.

If a value is missing, it defaults to padding 1.

See examples above, in 'fn' property.

## 's'

The 's' property means subtitle file, and is followed by a field that refers to a subtitle file or lyrics file.

Unfortunately, VLC only accepts local OS format to the subtitle file's path.

Ex.
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
?s?f:\media/Chinese\-Variety\Cooking Beauties 2018\Cooking.Beauties.01.vst

Note difference in file path format for VLC.

## 'd'

The 'd' property means duration that the media file will be played. Standard format is hours, minutes, seconds, milliseconds. If any of these values is omitted, that value is set to 0.

These values may need to be converted for some media players. For instance, VLC supports seconds only. Thus the plugin used in VLC needs to convert the standard h:m:s into seconds. A writer of VPL file should not do the conversion; they must write it in h:m:s:ms in order to remain compatible across all media players.


@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
:d:0:45

## 't'

The 't' property means title of the media file. Some media players may display the title on the playlist panel or window title bar.

Ex.
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
$t$Cooking Beauties 2018 - EP 01

## 'a'

The 'a' property means artist.

Ex.
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
$a$TVB actresses

## 'p'

The 'p' property means publisher.

Ex.
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
$p$TVB

## 'desc'

The 'desc' property means description -- a blurb to describe the media.

Ex.
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
$desc$Beautiful women celebrities compete in cooking contest. But will their cooking be delicious or horrendous?

## 'id'

The 'id' property sets the track's id -- its placement within the playlist. Usage depends on whether in 'idAuto' or 'idManual' mode.

The 'idAuto' property sets id assignment to auto. This is default mode, and doesn't have to used unless you want to return to auto mode from 'idManual'.

The 'idManual' property sets id assignment to manual.

In auto mode, each file on the playlist is automatically assgined an id, starting at 1 and incremented by 1 as new files are added. Use the 'id' property to assign a file's id to that value and start counting from there for subsequent files.

In manual mode, every subsequent file has id 0 unless you manually set each of their 'id' property.

Ex.
'idAuto
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.Preview.rmvb
$id$99
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
$id$1
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.02.rmvb
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.03.rmvb
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.04.rmvb

Here we enter id auto mode. We set the first file id to 99, which should put it at the end of the list. The second file is assigned id 1. The next file will be automatically assigned id 2, then next file id 3, etc. without need of further assignment.

Ex.
'idManual
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
$id$1
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.02.rmvb
$id$2
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.03.rmvb
$id$3

Here we enter id manual mode. We manually set every file's id. If we don't, those files will all have id 0.


## 'startplayid'

The 'startPlayId' property picks the id of the file to start playing when the playlist is first loaded. In the id auto mode, the id starts at 1, and ends at the number of files on the list. The default value for startPlayId is 1 -- the first item on the playlist. A startplayid value that doesn't match any id will start playing at the first file written in the VPL file.

Ex.
'startPlayId'3
'f'video-01.mp4
'f'video-02.mp4
'f'video-03.mp4
'f'video-04.mp4

will start playing at video-03.mp4.

## 'startPlayFile'

The 'startPlayFile' property matches the filename to start playing when the playlist is first loaded. The data supplied can be a partial file name.

'startPlayFile' is not processed if 'startPlayId' is also supplied.

Ex.
@startPlayFile@video-04
@f@video-01.mp4
@f@video-02.mp4
@f@video-03.mp4
@f@video-04.mp4


will start playing at video-04.mp4.

## 'startHere'

The 'startHere' property sets the current file id as the first file to be played. The last command found in the VPL between 'startHere' or 'startPlayId' will take priority. Either of these commands will take priority before 'startPlayFile'.

Ex.
@f@video-01.mp4
@f@video-02.mp4
@f@video-03.mp4
'startHere
@f@video-04.mp4


will start playing at video-03.mp4.

## 'func'

The 'func' properties allows passing special commands to the media player, including vendor specific options.

Ex.
'func'stayontop

# Global properties

The following properties apply the same data to all subsequent files if not specifically set for those files, until that property is invoked again to assign new data. Basically a fast way to assign the same data to an entire group of files, without repetitive text cluttering the playlist file, nor repetitive cut and paste.

Global properties are immediately assigned to any new file declared by 'f' or 'fn'.

'tAll' : same title
'aAll': same artist
'pAll': same publisher
'dAll': same duration
'descAll': same description
'pathAll': same path affixes

# Substitution

Data can also be substituted into other data by enclosing the corresponding property name inside double brackets (any of the header brackets are valid).

Replacements should be defined beforehand.

{{f}}: substitute file (full URL)
{{fpath}}: substitute file path (path to directory without protocol or filename)
{{ffull}}: substitute file name with extension
{{fname}}: substitute file name without extension
{{fext}}: substitute file extension (with leading dot)
{{s}}: substitute subtitle file (full URL)
{{t}}: substitute title
{{a}}: substitute artist
{{p}}: substitute publisher
{{d}}: substitute duration
{{id}}: substitute id
{{desc}}: substitute description

{{tAll}}: substitute global title
{{aAll}}: substitute global artist
{{pAll}}: substitute global publisher
{{descAll}}: substitute global description

Ex.
:tAll:Cooking Beauties
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
:t:{{tAll}}:part 01
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.02.rmvb
:t:{{tAll}}:part 02

will become:
Cooking Beauties - part 01
Cooking Beauties - part 02

Ex.
@f@Together.mp3
$a$S.H.E
$desc$Enjoy new hit single by {{a}}

In this case, 'a' should be defined before 'desc' can substitute it in. Else {{a}} will just be empty. You can also define global properties instead, also beforehand. So the following will also work:

Ex.
:aAll:S.H.E
@f@Together.mp3
$desc$Enjoy new hit single by {{a}}


In this case, 'f' will immediately receive global properties, such as 'aAll' before it. The latter implicitly defines 'a' for this file. Thus, {{a}} will be available for substitution.

To represent a property that will be substituted later, enclose the substitution in another layer of double enclosed brackets.

Code: [Select]
:tAll:Cooking Beauties:{{{{id}}}}
@f@Cooking.Beauties.01.rmvb
@f@Cooking.Beauties.02.rmvb

will become:
Cooking Beauties - 1
Cooking Beauties - 2

In this case, 'tAll' has a code {{{{id}}}}. When this line is processed, this will reduce it to just {{id}}. When 'f' is processed, 'tAll' is passed down and also processed, but now with just {{id}}, which of course refers to the current file's id.

# Variables

The 'var' property sets custom variables that can be substituted into other data by enclosing colon : and variable name in double brackets. The first value is the variable name. The second value is the desired text or number during substitution.

Ex.
:var:baseTitle:Cooking Beauties
'tAll'{{:baseTitle}}'{{{{id}}}}
'desc'{{:baseTitle}} features {{a}}

Here, we define a variable called 'baseTitle' with value of 'Cooking Beauties'. Later, we substitute the variable by its name into other properties, such as title and description. Note the colon prepending the variable name during substitution {{:baseTitle}}.

# Calculations

Do calculations on the spot. Enclose equal sign = and the equation in double brackets.

TBD: Notation used for math and logic. Presumably basic arithmetic + - * / are shared among most programming languages.

You may use variables instead of nested brackets. This is recommended method, to improve logic and readability.

Ex.
't'{{={{id}}-1}}

Ex.
'tAll'{{{{={{id}}-1}}}}

Ex.
(( Recommended method below, using variables to store equations
:var:newid:={{id}}-1
@tAll@Girls High School Mystery Class@{{{{:newid}}}}


# Full Examples

## Simple playlists
Ex.
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.02.rmvb
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.03.rmvb
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.04.rmvb


## Complex playlists
Ex.
@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
@t@Cooking Beauties 2018 - EP 01
:d:0:45
@a@TVB
@p@TVB
@desc@Beautiful women celebrities compete in cooking contest. But will their cooking be delicious or horrendous?

@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.02.rmvb
@t@Cooking Beauties 2018 - EP 02
:d:0:45
@a@TVB
@p@TVB
@desc@Beautiful women celebrities compete in cooking contest. But will their cooking be delicious or horrendous?

@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.03.rmvb
@t@Cooking Beauties 2018 - EP 03
:d:0:45
@a@TVB
@p@TVB
@desc@Beautiful women celebrities compete in cooking contest. But will their cooking be delicious or horrendous?

@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.04.rmvb
@t@Cooking Beauties 2018 - EP 04
:d:0:45
@a@TVB
@p@TVB
@desc@Beautiful women celebrities compete in cooking contest. But will their cooking be delicious or horrendous?


Ex.
(( The same as above, but much neater by using *All properties and fn
:var:baseTitle:Cooking Beauties 2018
,tAll,{{:baseTitle}},{{{{id}}}}
@aAll@TVB actresses
,pAll,TVB
@descAll@Beautiful women celebrities compete in cooking contest. But will their cooking be delicious or horrendous?
@commentAll@Beautiful Cooking returns to a third season after nine years of hiatus.
'pathAll'file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.'.rmvb
-pad-2
-fn-1-21

Ex.
,startPlayId,2
,tAll,Asheron's Call 2 Livestream ft. Shena'Fu,part {{{{id}}}}
,pathAll,https://www.youtube.com/watch?v=
@f@EZfbsT8FAQ4
@f@RysXIquKQwY
@f@iG-s9QhaQZQ
@f@NySeXRmeAEg


Ex.
(( Multiple levels of substitution:
{{Main}}

@tAll@Cooking Beauties 2018@{{{{id}}}}
@aAll@TVB
,pAll,TVB
@descAll@Beautiful women celebrities compete in cooking contest. But will their cooking be delicious or horrendous?@@{{{{t}}}} features {{{{a}}}}.
@commentAll@Beautiful Cooking returns to a third season after nine years of hiatus.

@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.01.rmvb
:d:0:45

@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.02.rmvb
:d:0:45

@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.03.rmvb
:d:0:45

@f@file:///f:/media/Chinese/-Variety/Cooking Beauties 2018/Cooking.Beauties.04.rmvb
@a@[[aAll]]@beauties
@p@[[pAll]]@partners
:d:0:45

# TBD

TBD: append supplementary playlists; should also append indexes

TBD: auto-generated files, i.e. 'smart' playlists. e.g. generate incremented ids in filenames (done); get all files in directory based on criteria

TBD: plugins for media players: Winamp

# Lbiraries and Plugins

## VLC
VPL playlist can be loaded in VLC with this Lua extension:
[https://bitbucket.org/Shenafu/vsv/src/master/playlist/] (https://bitbucket.org/Shenafu/vsv/src/master/playlist/)
	</pre>
