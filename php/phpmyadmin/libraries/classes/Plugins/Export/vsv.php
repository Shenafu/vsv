<?php
/*
 * VSV
 */

namespace PhpMyAdmin\Plugins\Export;

class vsv {
	const HEADER = "HEADER";
	const DATA = "DATA";
	const FILETYPE = "FILETYPE";
	const FIELDOPENER = "/^(\[\[|\(\(|\{\{|\<\<)/";
	const FIELDBRACKETS = "/[\{\(\[\<]{2}(.*?)[\}\)\]\>]{2}/";
	const FILEHEADER = "/[\{\(\[\<]{2}meta[\}\)\]\>]{2}/";
	const DEFAULTDELIMS = "|`=,.-:;"; // default delimiters
	const DEFAULTBRACKETS = '[](){}<>';

	public $text;

	function __construct($input="") {
		$this->text = $input;
	}

	public function mapToArray($keepDelimiter = false) {
		// convert text rows into array
		// each array item is subarray of header or data items
		// each subarray's 0th index is 'header' or 'data'

		$this->array = [];
		$rows = explode( "\n", $this->text);

		foreach ($rows as $row) {
			$row = trim($row);
			if (0 == strlen($row)) {
				continue;
			}

			if (preg_match(self::FIELDOPENER, $row)) {
				// is header row
				// get all fields into array
				preg_match(self::FIELDBRACKETS, $row, $matches);
				if ($matches) {
					array_unshift($matches, 'header');
				}
			}
			else {
				// is data row
				$delimiter = substr($row, 0, 1);
				$row = substr($row, 1); // ignore first delimiter
				// ignore if last character is delimiter
				if ($delimiter == substr($row, -1)) {
					$row = substr($row, 0, -1);
				}
				$matches = explode($delimiter, $row);
				if ($matches) {
					if ($keepDelimiter) {
						array_unshift($matches, $delimiter);
					}
					array_unshift($matches, 'data');
				}
			}
			if ($matches) {
				$this->array[] = $matches;
			}
		}

		return $this->array;
	}

	public function mapToHash () {
		// convert data rows into hash object
		// row's first value = key
		// row's second value = value

		$rows = $this->mapToArray();
		$this->hash = [];

		foreach ($rows as $row) {
			$rowType = array_shift($row);
			switch ($rowType) {
				case 'header':
					// is header row

					// TODO: do we need header fields?
					break;

				case 'data':
					// is data row
					$key = array_shift($row);
					if (1 > count($row)) {
						// no data value
						$this->hash[$key] = "";
					}
					elseif (1 == count($row)) {
						// one data value
						$this->hash[$key] = $row[0];
					}
					else {
						// many data values, put into subarray
						foreach ($row as $data) {
							$this->hash[$key][] = $data;
						}
					}
					break;
			} // end switch rowType
		}

		return $this->hash;
	}

	public function mapToAtomic () {
		// convert each data value in its own array index
		$rows = $this->mapToArray();
		$this->atomic = [];

		foreach ($rows as $row) {
			$rowType = array_shift($row);
			switch ($rowType) {
				case 'header':
					// is header row

					// TODO: do we need header fields?
					break;

				case 'data':
					// is data row
					foreach ($row as $data) {
						$this->atomic[] = $data;
					}
					break;
			} // end switch rowType
		}

		return $this->atomic;
	}

	public function mapFromVon () {
		// convert VON into hiearchical array or object recursively
		$rows = $this->mapToArray(true);
		$container = vsv::vonRecurse([], $rows);
		$this->von = $container[0];
		return $this->von;
	}

	private static function vonRecurse ($parent, &$rows) {
		while(!empty($rows)) {
			$row = array_shift($rows);
			$rowType = array_shift($row);
			switch ($rowType) {
				case 'header':
					// ignore header rows
					break;

				case 'data':
					// is data row
					$delim = array_shift($row);
					switch ($delim) {
						case '}':
						case ']':
						case ')':
							// close current container
							if ($delim == ')') {
								$parent .= " }";
							}
							return $parent;

						case '{':
						case '[':
						case '(':
							// add new object or array or function to tree
							if ($delim == '{') {
								if ('}' == substr($row[0], -1)) {
									// inline object
									$list = substr(preg_replace("/^ */", '', $row[0]), 0, -1);
									$arrayDelim = substr($list, 0, 1);
									$subArray = explode($arrayDelim, $list);
									array_shift($subArray);
									$newObject = new stdClass;
									// if parent is object, first data is key name
									if (is_object($parent)) {
										$key = array_shift($subArray);
										$parent->{$key} = $newObject;
									}
									// each pair in subArray is (key, value)
									for ($i=0; isset($subArray[$i]); $i+=2) {
										$key = $subArray[$i];
										$newObject->{$key} = $subArray[$i+1];
									}
									if (is_array($parent)) {
										$parent[] = $newObject;
									}
									break;
								}
								else {
									$ret = vsv::vonRecurse(new stdClass, $rows);
								}
							}
							elseif ($delim == '[') {
								if (']' == substr($row[0],-1)) {
									// inline array
									$list = substr(preg_replace("/^ */", '', $row[0]), 0, -1);
									$arrayDelim = substr($list, 0, 1);
									$subArray = explode($arrayDelim, $list);
									array_shift($subArray);
									// if parent is object, first data is key name
									if (is_object($parent)) {
										$key = array_shift($subArray);
										$parent->{$key} = $subArray;
									}
									else {
										$parent[] = $subArray;
									}
									break;
								}
								else {
									$ret = vsv::vonRecurse([], $rows);
								}
							}
							elseif ($delim == '(') {
								$ret = vsv::vonRecurse("function", $rows);
							}

							// append to parent container
							if (is_array($parent)) {
								// last is array
								$parent[] = $ret;
							}
							else {
								// last is object
								$parent->{$row[0]} = $ret;
							}
							break;

						case '<':
							// add arguments to function
							$parent .= "( ";
							break;

						case '>':
							// close arguments to function
							$parent .= " ) {";
							break;

						default:
							if (is_string($parent)){
								// parent is function, append data to end
								$parent .= implode($row);
								break;
							}
							elseif (is_array($parent)) {
								// parent is array, so append each data
								// where row is in the form ,data,data,data
								foreach ($row as $data) {
									$parent[] = $data;
								}
								break;
							}
							elseif (is_object($parent)) {
								// parent is object, add (key, value)
								$key = array_shift($row);
								$parent->{$key} = (0 < count($row)) ? implode(', ', $row) : "";
								break;
							}

					} // end switch delim
			} // end switch rowType
		}

		return $parent;
	}

}
?>