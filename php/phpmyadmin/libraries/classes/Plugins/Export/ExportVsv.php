<?php
/* vim: set expandtab sw=4 ts=4 sts=4: */
/**
 * VSV export code
 *
 * @package    PhpMyAdmin-Export
 * @subpackage VSV
 */

namespace PhpMyAdmin\Plugins\Export;

use PhpMyAdmin\DatabaseInterface;
use PhpMyAdmin\Export;
use PhpMyAdmin\Plugins\ExportPlugin;
use PhpMyAdmin\Properties\Plugins\ExportPluginProperties;
use PhpMyAdmin\Properties\Options\Groups\OptionsPropertyMainGroup;
use PhpMyAdmin\Properties\Options\Groups\OptionsPropertyRootGroup;
use PhpMyAdmin\Properties\Options\Items\BoolPropertyItem;
use PhpMyAdmin\Properties\Options\Items\HiddenPropertyItem;
use PhpMyAdmin\Properties\Options\Items\TextPropertyItem;

/**
 * Handles the export for the VSV format
 *
 * @package    PhpMyAdmin-Export
 * @subpackage VSV
 */
class ExportVsv extends ExportPlugin
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->setProperties();
    }

    /**
     * Sets the export VSV properties
     *
     * @return void
     */
    protected function setProperties()
    {
        $exportPluginProperties = new ExportPluginProperties();
        $exportPluginProperties->setText('VSV');
        $exportPluginProperties->setExtension('vsv');
        $exportPluginProperties->setMimeType('text/versatile-separated-values');
        $exportPluginProperties->setOptionsText(__('Options'));

        // create the root group that will be the options field for
        // $exportPluginProperties
        // this will be shown as "Format specific options"
        $exportSpecificOptions = new OptionsPropertyRootGroup(
            "Format Specific Options"
        );

        // general options main group
        $generalOptions = new OptionsPropertyMainGroup("general_opts");
        // create leaf items and add them to the group

        $leaf = new TextPropertyItem(
            "delimiters",
            __('Delimiters in order:')
        );
        $generalOptions->addProperty($leaf);

        $leaf = new HiddenPropertyItem(
            'structure_or_data'
        );
        $generalOptions->addProperty($leaf);

        // add the main group to the root group
        $exportSpecificOptions->addProperty($generalOptions);

        // set the options for the export plugin property item
        $exportPluginProperties->setOptions($exportSpecificOptions);
        $this->properties = $exportPluginProperties;
    }

    /**
     * Outputs export header
     *
     * @return bool Whether it succeeded
     */
    public function exportHeader()
    {
        global $what, $cfg, $vsv_delimiters;

        // Here we just prepare some values for export

        // prepare delimiters
        if (!isset($vsv_delimiters)) {
				$vsv_delimiters = $cfg['Export']['vsv_delimiters'];
        }

			$vsv_delimiters = str_replace('\\t', "\011", $vsv_delimiters);

        return true;
    }

    /**
     * Outputs export footer
     *
     * @return bool Whether it succeeded
     */
    public function exportFooter()
    {
        return true;
    }

    /**
     * Outputs database header
     *
     * @param string $db       Database name
     * @param string $db_alias Alias of db
     *
     * @return bool Whether it succeeded
     */
    public function exportDBHeader($db, $db_alias = '')
    {
        return true;
    }

    /**
     * Outputs database footer
     *
     * @param string $db Database name
     *
     * @return bool Whether it succeeded
     */
    public function exportDBFooter($db)
    {
        return true;
    }

    /**
     * Outputs CREATE DATABASE statement
     *
     * @param string $db          Database name
     * @param string $export_type 'server', 'database', 'table'
     * @param string $db_alias    Aliases of db
     *
     * @return bool Whether it succeeded
     */
    public function exportDBCreate($db, $export_type, $db_alias = '')
    {
        return true;
    }

    /**
     * Outputs the content of a table in VSV format
     *
     * @param string $db        database name
     * @param string $table     table name
     * @param string $crlf      the end of line sequence
     * @param string $error_url the url to go back in case of error
     * @param string $sql_query SQL query for obtaining data
     * @param array  $aliases   Aliases of db/table/columns
     *
     * @return bool Whether it succeeded
     */
    public function exportData(
        $db,
        $table,
        $crlf,
        $error_url,
        $sql_query,
        array $aliases = array()
    ) {
        global $what, $vsv_delimiters;

        $db_alias = $db;
        $table_alias = $table;
        $this->initAlias($aliases, $db_alias, $table_alias);

        // Gets the data from the database
        $result = $GLOBALS['dbi']->query(
            $sql_query,
            DatabaseInterface::CONNECT_USER,
            DatabaseInterface::QUERY_UNBUFFERED
        );
        $fields_cnt = $GLOBALS['dbi']->numFields($result);

        // Put all data into array for VSV
        $vsvArray = array();

        // write header row with column names
			$vsvHeader = array(vsv::HEADER);
			for ($i = 0; $i < $fields_cnt; $i++) {
					$col_as = $GLOBALS['dbi']->fieldName($result, $i);
					if (!empty($aliases[$db]['tables'][$table]['columns'][$col_as])) {
						$col_as = $aliases[$db]['tables'][$table]['columns'][$col_as];
					}
					$col_as = stripslashes($col_as);
					$vsvHeader[] = $col_as;
			} // end for
			$vsvArray[] = $vsvHeader;

			// write data rows
        while ($row = $GLOBALS['dbi']->fetchRow($result)) {
				$vsvData = array(vsv::DATA);
            for ($j = 0; $j < $fields_cnt; $j++) {
                if (isset($row[$j]) && !is_null($row[$j])) {
						// remove CRLF characters within field
							$row[$j] = strtr($row[$j],
								array(
									"\n"=>""
									, "\r"=>""
								)
							);
							$vsvData[] = $row[$j];
                }
                else {
							$vsvData[] = '';
                }
            } // end for
            $vsvArray[] = $vsvData;
        } // end while
        $GLOBALS['dbi']->freeResult($result);


        $openBrackets = "((";
        $closeBrackets = "))";
        $signature = "[[VSV]] [[table]]\n";
        $schema_insert = $signature;

        foreach ($vsvArray as $row) {
            $rowType = array_shift($row);
            switch ($rowType) {

					case vsv::HEADER:
						foreach ($row as $field) {
							$schema_insert .= $openBrackets . $field . $closeBrackets . " ";
						}
						break;

					case vsv::DATA:

						// find a good delimiter
						$rowString = implode("", $row);
						for ($d = 0; $d < strlen($vsv_delimiters); $d++) {
							$delimiter = substr($vsv_delimiters, $d, 1);
							if (!strstr($rowString, $delimiter)) {
								break;
							}
						}
						foreach ($row as $field) {
							$schema_insert .= $delimiter . $field;
						}
						break;
				}
            $schema_insert .= "\n";
        }
			if (!Export::outputHandler($schema_insert)) {
					return false;
			}

        return true;
    }
}
