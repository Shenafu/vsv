<?php

/*
 * VSV Test
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


require_once($_SERVER['DOCUMENT_ROOT']."/lib/autoload.php");
spl_autoload("vsv_dev");

$files = [
$_SERVER['DOCUMENT_ROOT']."/code/vsv/docs/sample.von",
$_SERVER['DOCUMENT_ROOT']."/magic/custommana/business.vsv",
$_SERVER['DOCUMENT_ROOT']."/magic/custommana/business.von",
];
$datas = [];
foreach ($files as $file) {
	if (file_exists($file)) {
		$datas[] = file_get_contents($file, true);
	}
}

$vsv = new vsv($datas[1]);
$von = new vsv($datas[2]);
$von2 = new vsv();
$von2->text = $datas[0];

echo "<pre style='white-space: pre-wrap'>";

echo "============================\n";
echo "mapToAtomic():";
$codes = $vsv->mapToAtomic();
print_r($codes);

echo "============================\n";
echo "mapToHash():";
$codes = $vsv->mapToHash();
print_r($codes);

echo "============================\n";
echo "mapFromVon():\n";
$codes = $von->mapFromVon();
print_r($codes);

echo "============================\n";
echo "mapFromVon():\n";
$codes = $von2->mapFromVon();
print_r($codes);

echo "</pre>";

?>